package com.rohanisuhadi.vibook

import android.os.SystemClock
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.rohanisuhadi.vibook.splashscreen.SplashActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainApplicationTest {

    private val delay = 2000L

    @JvmField
    @Rule
    val mainActivityRule: ActivityTestRule<SplashActivity> = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun startApplication() {

        //testing login false
        SystemClock.sleep(delay)
        Espresso.onView(ViewMatchers.withId(R.id.email)).perform(ViewActions.click()).perform(
            ViewActions.typeText("admin")
        ).perform(ViewActions.closeSoftKeyboard())
        SystemClock.sleep(delay)
        Espresso.onView(ViewMatchers.withId(R.id.password)).perform(ViewActions.click()).perform(
            ViewActions.typeText("admin")
        ).perform(ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.login)).perform(ViewActions.click())
        SystemClock.sleep(delay)
        SystemClock.sleep(delay)

//        testing login true
        Espresso.onView(withId(R.id.email)).perform(ViewActions.clearText())
        Espresso.onView(withId(R.id.password)).perform(ViewActions.clearText())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.email)).perform(ViewActions.click()).perform(
            ViewActions.typeText("postman")
        ).perform(ViewActions.closeSoftKeyboard())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.password)).perform(ViewActions.click()).perform(
            ViewActions.typeText("password")
        ).perform(ViewActions.closeSoftKeyboard())
        Espresso.onView(withId(R.id.login)).perform(ViewActions.click())
        SystemClock.sleep(delay)
        SystemClock.sleep(delay)
        SystemClock.sleep(delay+delay+delay+delay)

        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        SystemClock.sleep(delay)
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        Espresso.onView(withId(R.id.nestedScrollViewBook)).perform(swipeUp())
        SystemClock.sleep(delay)




    }
}