package com.rohanisuhadi.vibook.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rohanisuhadi.vibook.R

class HomeHolder (v: View) : RecyclerView.ViewHolder(v) {

    var tvTitle: TextView
    var tvDescription: TextView
    var ivBook: ImageView

    init {
        tvTitle = itemView.findViewById(R.id.textViewTitle) as TextView
        tvDescription = itemView.findViewById(R.id.textViewDescrition) as TextView
        ivBook = itemView.findViewById(R.id.imageViewBook) as ImageView
    }

}