package com.rohanisuhadi.vibook.network

import com.rohanisuhadi.vibook.model.Book
import com.rohanisuhadi.vibook.model.Login
import retrofit2.http.GET

interface APIService {

    @GET("basic-auth")
    fun doLogin(): io.reactivex.Observable<Login>

    @GET("Books")
    fun getBooks():io.reactivex.Observable<List<Book>>

}