package com.rohanisuhadi.vibook.network

import com.rohanisuhadi.vibook.model.Book

class APINetworkBook {

    private val api = APIClientBook.create()

    fun getBook(): io.reactivex.Observable<List<Book>>{
        return  api.getBooks()
    }

}