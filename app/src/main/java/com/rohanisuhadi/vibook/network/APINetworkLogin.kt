package com.rohanisuhadi.vibook.network

import com.rohanisuhadi.vibook.model.Login
import com.rohanisuhadi.vibook.model.User


class APINetworkLogin {

    fun doLogin(user: User): io.reactivex.Observable<Login>{
        return  APIClientLogin.create(user).doLogin()
    }

}