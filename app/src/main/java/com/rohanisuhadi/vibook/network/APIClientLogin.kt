package com.rohanisuhadi.vibook.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rohanisuhadi.vibook.BuildConfig
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import android.util.Base64.NO_WRAP
import android.R.attr.password
import android.util.Base64
import com.rohanisuhadi.vibook.model.User


object APIClientLogin {

    val BASE_URL = BuildConfig.NETWORK_HOST_LOGIN

    lateinit var retrofit: Retrofit

    fun create(user: User): APIService {

        val gson = getGsonInstance()
        val interceptor = HttpLoggingInterceptor()

        if(BuildConfig.DEBUG)
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        else
            interceptor.level = HttpLoggingInterceptor.Level.NONE

        val httpClient = OkHttpClient.Builder().addInterceptor(interceptor)

        val credentials =  "${user.username}:${user.password}"

        val basic = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)

        httpClient.addInterceptor { chain ->
            chain?.proceed(chain.request()?.newBuilder()?.addHeader("Accept", "application/json")?.build())
        }

        httpClient.addInterceptor { chain ->
            chain?.proceed(chain.request()?.newBuilder()?.addHeader("Authorization", basic)?.build())
        }

        val client = httpClient.build()

        retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(APIService::class.java)
    }

    fun getGsonInstance(): Gson {
        return GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
    }

}