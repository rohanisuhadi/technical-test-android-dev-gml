package com.rohanisuhadi.vibook.login

interface ContractLogin {

    interface View{
        fun showMessage(message:String)
        fun successLogin()
        fun errorLogin()
        fun setLoading(status: Boolean)
    }

    interface Presenter{
        fun loginAPI(email:String, password:String)
    }

}