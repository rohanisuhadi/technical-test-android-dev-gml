package com.rohanisuhadi.vibook.login

import com.rohanisuhadi.vibook.model.Login
import com.rohanisuhadi.vibook.model.User
import com.rohanisuhadi.vibook.network.APINetworkLogin
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class PresenterLogin(_view : ContractLogin.View ) : ContractLogin.Presenter {

    private var view: ContractLogin.View = _view
    private val user: User = User()

    override fun loginAPI(_username: String, _password: String) {
        user.username = _username
        user.password = _password

        APINetworkLogin().doLogin(user).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<Login>() {

                override fun onStart() {
                    view.setLoading(true)
                }

                override fun onNext(baseEntity: Login) {
                    view.successLogin()
                }

                override fun onError(e: Throwable) {
                    view.showMessage(e.message.toString())
                    view.errorLogin()
                    view.setLoading(false)
                }

                override fun onComplete() {
                    view.setLoading(false)
                }
            })

    }

}