package com.rohanisuhadi.vibook.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.rohanisuhadi.vibook.R
import com.rohanisuhadi.vibook.home.HomeActivity


class LoginActivity : AppCompatActivity(), ContractLogin.View {

    private var presenter: PresenterLogin ?= null

    private var btnLogin: Button ?= null
    private var txtEmail: TextView ?= null
    private var txtPassword: TextView ?= null
    private var progressBar: ProgressBar ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = PresenterLogin(this)

        btnLogin = findViewById(R.id.login) as Button
        txtEmail = findViewById(R.id.email) as TextView
        txtPassword = findViewById(R.id.password) as TextView
        progressBar = findViewById(R.id.progressBar) as ProgressBar

        btnLogin?.setOnClickListener { presenter?.loginAPI(txtEmail?.text.toString(), txtPassword?.text.toString()) }
    }

    private fun startHome(){
        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this@LoginActivity, message, Toast.LENGTH_LONG).show()
    }

    override fun successLogin() {
        startHome()
    }

    override fun setLoading(status: Boolean) {
        if(status)
            progressBar?.visibility = View.VISIBLE
        else
            progressBar?.visibility = View.GONE
    }

    override fun errorLogin() {

    }

}
