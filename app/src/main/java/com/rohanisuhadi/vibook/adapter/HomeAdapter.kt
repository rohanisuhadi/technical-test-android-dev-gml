package com.rohanisuhadi.vibook.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rohanisuhadi.vibook.R
import com.rohanisuhadi.vibook.holder.HomeHolder
import com.rohanisuhadi.vibook.model.Book

class HomeAdapter(private var books : ArrayList<Book>, private var context: Context) : RecyclerView.Adapter<HomeHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHolder {
        return HomeHolder(LayoutInflater.from(context).inflate(R.layout.list_home, parent, false))
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: HomeHolder, position: Int) {
        holder.tvTitle.text = books.get(position).title
        holder.tvDescription.text = books.get(position).description
        Glide.with(context).load("https://cdn.pixabay.com/photo/2018/11/02/13/24/autumn-3790244_960_720.jpg")
            .apply(RequestOptions()).into(holder.ivBook)

    }
}