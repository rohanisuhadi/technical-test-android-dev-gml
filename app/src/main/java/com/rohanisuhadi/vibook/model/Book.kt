package com.rohanisuhadi.vibook.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class Book (

	@field:SerializedName("PublishDate")
	val publishDate: String? = null,

	@field:SerializedName("Description")
	val description: String? = null,

	@field:SerializedName("PageCount")
	val pageCount: Int? = null,

	@field:SerializedName("Excerpt")
	val excerpt: String? = null,

	@field:SerializedName("Title")
	val title: String? = null,

	@field:SerializedName("ID")
	val iD: Int? = null
): Serializable