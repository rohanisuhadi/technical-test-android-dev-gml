package com.rohanisuhadi.vibook.model

import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("username")
    var username: String ?= null
    @SerializedName("password")
    var password: String ?= null

    override fun toString(): String {
        return "User(username=$username, password=$password)"
    }

}