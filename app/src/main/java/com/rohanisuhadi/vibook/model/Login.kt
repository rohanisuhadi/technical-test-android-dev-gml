package com.rohanisuhadi.vibook.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class Login(
	@field:SerializedName("authenticated")
	val authenticated: Boolean? = null
): Serializable
