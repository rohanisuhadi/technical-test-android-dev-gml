package com.rohanisuhadi.vibook.home

import com.rohanisuhadi.vibook.model.Book
import com.rohanisuhadi.vibook.network.APINetworkBook
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class PresenterHome(_view: ContractHome.View) : ContractHome.Presenter {

    var books = ArrayList<Book>()
    private var view: ContractHome.View = _view

    override fun getBooks() {
        APINetworkBook().getBook().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<List<Book>>() {

                override fun onStart() {
                    view.setLoading(true)
                }

                override fun onNext(_books: List<Book>) {
                    books.addAll(_books)
                    view.updateView()
                }

                override fun onError(e: Throwable) {
                    view.showMessage(e.message.toString())
                    view.setLoading(false)
                }

                override fun onComplete() {
                    view.setLoading(false)
                }
            })
    }


}