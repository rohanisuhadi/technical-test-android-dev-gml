package com.rohanisuhadi.vibook.home

interface ContractHome {

    interface View{
        fun showMessage(message:String)
        fun setLoading(status: Boolean)
        fun updateView()
    }

    interface Presenter{
        fun getBooks()
    }

}