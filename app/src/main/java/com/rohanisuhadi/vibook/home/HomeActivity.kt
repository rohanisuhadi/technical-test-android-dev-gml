package com.rohanisuhadi.vibook.home

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.slider.library.Animations.BaseAnimationInterface
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.rohanisuhadi.vibook.R
import com.rohanisuhadi.vibook.adapter.HomeAdapter

import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), ContractHome.View,  BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener  {


    private var presenter: PresenterHome ?= null
    private var recyclerViewBook:RecyclerView ?= null
    private var adapterBook: RecyclerView.Adapter<*> ?= null
    private var imageSlider: SliderLayout?= null
    private var progressBar: ProgressBar? = null
    private var nestedScrollViewBook: NestedScrollView?= null

    private var loadData = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        presenter = PresenterHome(this)

        recyclerViewBook = findViewById(R.id.recyclerViewBook) as RecyclerView
        imageSlider = findViewById(R.id.sliderLayout) as SliderLayout
        progressBar = findViewById(R.id.progressBar) as ProgressBar
        nestedScrollViewBook = findViewById(R.id.nestedScrollViewBook) as NestedScrollView

        adapterBook = presenter?.books?.let { HomeAdapter(it,this@HomeActivity) }

        recyclerViewBook?.layoutManager =  LinearLayoutManager(this@HomeActivity)
        recyclerViewBook?.adapter = adapterBook
        recyclerViewBook?.setNestedScrollingEnabled(false)

        createImageSlider()

        initLoadMore()

        presenter?.getBooks()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_notification -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showMessage(message: String) {

    }

    override fun setLoading(status: Boolean) {
        if(status)
            progressBar?.visibility = View.VISIBLE
        else
            progressBar?.visibility = View.GONE
    }

    override fun updateView() {
        loadData = true
        recyclerViewBook?.adapter?.notifyDataSetChanged()
    }

    private fun createImageSlider(){
        for(i in 0..2){
            val textSliderView = TextSliderView(this)
            textSliderView
                .description("Belajar")
                .image("https://storage.googleapis.com/audiobukucover/3c3ea117-74de-4dc9-96ff-7dcf9fe59798.jpg")
                .setScaleType(BaseSliderView.ScaleType.Fit)

            textSliderView.bundle(Bundle())
            imageSlider?.addSlider(textSliderView)
            imageSlider?.setPresetTransformer(SliderLayout.Transformer.Accordion)
            imageSlider?.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            imageSlider?.setCustomAnimation(DescriptionAnimation() as BaseAnimationInterface?)
            imageSlider?.setDuration(4000)
            imageSlider?.addOnPageChangeListener(this)
        }

    }

    private fun initLoadMore() {
        nestedScrollViewBook?.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener{
            override fun onScrollChange( v:NestedScrollView,  scrollX:Int, scrollY:Int, oldScrollX:Int, oldScrollY:Int) {
                
                val BOTTOM_HEIGHT = (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )

                if ( BOTTOM_HEIGHT <= scrollY && loadData) {
                    loadData = false
                    presenter?.getBooks()
                }
            }
        })
    }

    // implement image slider
    override fun onSliderClick(slider: BaseSliderView?) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

}
